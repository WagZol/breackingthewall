## Function Description
Complete aFal method, what checks if a given wall length(num of block) can be build from a given amaount of block of lrngth 5 and blcck of length 1

test has the following parameter(s):
*	falHossz: an int as the lenght of wall
*   otosok:   an int as amount of block of length 5
*   egyesek:  an int as amount of block of legnth 1

## Input Format
A three integers.

## Constraints
*	0<=falHossz<=Integer.MAX_VALUE(2147483647)
*	0<=otosok<=Integer.MAX_VALUE(2147483647)
*	0<=egyesek<=Integer.MAX_VALUE(2147483647)

## Output Format
Return boolean true if wall can be build, otherwise, return boolean false.

## Sample Input 0
```12, 20, 5```
## Sample Output 0
```true```

## Explanation 0
Given ***falHossz*** **= 12**, we we could build the wall from 2 block of length 5 and 2 block of length 1, and the given ***otosok*** **= 20** and the given ***egyesek*** **= 5** so we have enough blocks to build the wall, so the output is true

## Sample Input 1
```23, 5, 2```

## Sample Output 1
```false```

## Explanation 1
Given ***falHossz*** **= 23**, we we could build the wall from 4 block of length 5 and 3 block of length 1, and the given ***otosok*** **= 5** and the given ***egyesek*** **= 2** so we don't have enough blocks to build the wall, so the output is false
